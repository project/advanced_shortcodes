/**
 * Open / close the accordion when label is focused and spacebar pressed.
 */
document.addEventListener('DOMContentLoaded', function () {
  // Only do it for accordions from advanced_shortcodes.
  document.querySelectorAll('.advanced-shortcodes-accordion .accordion-tab-label').forEach(label => {
    label.addEventListener('keydown', function (event) {
      // Check if the key pressed is 'Space'.
      if (event.key === ' ' || event.keyCode === 32) {
        // Prevent the default action to stop scrolling the page.
        event.preventDefault();
        var checkbox = document.getElementById(this.getAttribute('for'));
        // Toggle the checkbox.
        checkbox.checked = !checkbox.checked;
      }
    });
  });
});
  