<?php

namespace Drupal\advanced_shortcodes\Plugin\Shortcode;

use Drupal\Core\Language\Language;
use Drupal\shortcode\Plugin\ShortcodeBase;

/**
 * The accordions shortcode.
 *
 * @Shortcode(
 *   id = "accordions",
 *   title = @Translation("Accordions Container"),
 *   description = @Translation("Accordions container")
 * )
 */
class AccordionsShortcode extends ShortcodeBase {

  /**
   * {@inheritdoc}
   */
  public function process($attributes, $text, $langcode = Language::LANGCODE_NOT_SPECIFIED) {

    $attributes['class'] = $attributes['class'] ?? 'default';
    $attributes['class'] .= ' accordion accordion-tab';
    $attributes['id'] = uniqid();
    $output = [
      '#theme' => 'shortcode_accordions',
      '#attributes' => $attributes,
      '#text' => $text,
    ];
    return $this->render($output);
  }

  /**
   * Remove white space from render value.
   */
  public function tips($long = FALSE) {
    $output = [];
    $output[] = '<p><strong>' . $this->t('[accordions (class="additional class")](text)[/accordions]') . '</strong></p> ';
    return implode(' ', $output);
  }

}
