<?php

namespace Drupal\advanced_shortcodes\Plugin\Shortcode;

use Drupal\Core\Language\Language;
use Drupal\shortcode\Plugin\ShortcodeBase;

/**
 * The progress shortcode class.
 *
 * @Shortcode(
 *   id = "progress",
 *   title = @Translation("Progress Bar"),
 *   description = @Translation("Progress Bar line"),
 * )
 */
class ProgressShortcode extends ShortcodeBase {

  /**
   * {@inheritdoc}
   */
  public function process($attributes, $text, $langcode = Language::LANGCODE_NOT_SPECIFIED) {

    $percent = isset($attributes['percent']) && $attributes['percent'] ? $attributes['percent'] : 0;
    $output = [
      '#theme' => 'shortcode_progress',
      '#attributes' => $attributes,
      '#value' => $percent,
    ];
    return $this->render($output);
  }

  /**
   * Remove white space from render value.
   */
  public function tips($long = FALSE) {
    $output = [];
    $output[] = '<p><strong>' . $this->t('[progress (percent="50" class="additional class")][/progress]') . '</strong></p> ';
    return implode(' ', $output);
  }

}
