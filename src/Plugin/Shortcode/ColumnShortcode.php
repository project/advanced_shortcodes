<?php

namespace Drupal\advanced_shortcodes\Plugin\Shortcode;

use Drupal\Core\Language\Language;
use Drupal\shortcode\Plugin\ShortcodeBase;

/**
 * The column shortcode class.
 *
 * @Shortcode(
 *   id = "column",
 *   title = @Translation("Column"),
 *   description = @Translation("Bootstrap Column"),
 * )
 */
class ColumnShortcode extends ShortcodeBase {

  /**
   * {@inheritdoc}
   */
  public function process($attributes, $text, $langcode = Language::LANGCODE_NOT_SPECIFIED) {
    $attributes = $this->getAttributes([
      'class' => '',
      'cols'  => '',
      'begin' => '',
      'end'   => '',
      'xs' => '',
      'sm' => '',
      'md' => '',
      'lg' => '',
    ],
      $attributes
    );
    if ($attributes['cols'] && is_numeric($attributes['cols'])) {
      // For xs, use full 12 columns. All others,use the "cols" value.
      $attributes['xs'] = $attributes['xs'] ? $attributes['xs'] : '12';
      $attributes['sm'] = $attributes['sm'] ? $attributes['sm'] : $attributes['cols'];
      $attributes['md'] = $attributes['md'] ? $attributes['md'] : $attributes['cols'];
      $attributes['lg'] = $attributes['lg'] ? $attributes['lg'] : $attributes['cols'];
    }

    $class = $attributes['class'];
    foreach (['xs', 'sm', 'md', 'lg'] as $size) {
      if ($attributes[$size]) {
        $class = $this->addClass($class, 'col-' . $size . '-' . $attributes[$size]);
      }
    }
    $attributes['class'] = $class ?? '';
    $output = [
      '#theme' => 'shortcode_column',
      '#attributes' => $attributes,
      '#text' => $text,
      '#begin_row' => $attributes['begin'] ? '<div class="row">' : '',
      '#end_row' => $attributes['end'] ? '</div>' : '',
    ];
    return $this->render($output);
  }

  /**
   * Remove white space from render value.
   */
  public function tips($long = FALSE) {
    $output = [];
    $output[] = '<p><strong>' . $this->t('[column (class="col-lg-6 col-md-6 col-sm-6 col-xs-6")][/column]') . '</strong></p> ';
    return implode(' ', $output);
  }

}
