<?php

namespace Drupal\advanced_shortcodes\Plugin\Shortcode;

use Drupal\Core\Language\Language;
use Drupal\shortcode\Plugin\ShortcodeBase;

/**
 * The accordion shortcode.
 *
 * @Shortcode(
 *   id = "accordion",
 *   title = @Translation("Accordion Item"),
 *   description = @Translation("Accordion Item"),
 *
 * )
 */
class AccordionShortcode extends ShortcodeBase {

  /**
   * {@inheritdoc}
   */
  public function process($attributes, $text, $langcode = Language::LANGCODE_NOT_SPECIFIED) {

    $icon = isset($attributes['icon']) && $attributes['icon'] ? '<i class="' . $attributes['icon'] . '"></i> ' : '';
    $attributes['class'] = $attributes['class'] ?? '';
    $attributes['class'] .= " accordion-item advanced-shortcodes-accordion";
    $title = $attributes['title'] ?? '';
    $attributes['title'] = strip_tags($title);
    $id = uniqid();
    $output = [
      '#theme' => 'shortcode_accordion',
      '#attributes' => $attributes,
      '#text' => $text ,
      '#id' => $id ,
      '#icon' => $icon ,
      '#title' => $title,
    ];
    return $this->render($output);

  }

  /**
   * Remove white space from render value.
   */
  public function tips($long = FALSE) {
    $output = [];
    $output[] = '<p><strong>' . $this->t('[accordion  (title="Accordion title" icon="class icon name" class="additional class")](text)[/accordion]') . '</strong></p> ';
    return implode(' ', $output);
  }

}
