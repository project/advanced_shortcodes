<?php

namespace Drupal\advanced_shortcodes\Plugin\Shortcode;

use Drupal\Core\Language\Language;
use Drupal\shortcode\Plugin\ShortcodeBase;

/**
 * The icon shortcode class.
 *
 * @Shortcode(
 *   id = "icon",
 *   title = @Translation("Icon"),
 *   description = @Translation("Icon"),
 * )
 */
class IconShortcode extends ShortcodeBase {

  /**
   * {@inheritdoc}
   */
  public function process($attributes, $text, $langcode = Language::LANGCODE_NOT_SPECIFIED) {
    $output = [
      '#theme' => 'shortcode_icon',
      '#attributes' => $attributes,
      '#text' => $text,
    ];
    return $this->render($output);

  }

  /**
   * Remove white space from render value.
   */
  public function tips($long = FALSE) {
    $output = [];
    $output[] = '<p><strong>' . $this->t('[icon (class="additional class")]text[/icon]') . '</strong></p> ';
    return implode(' ', $output);
  }

}
