Advanced shortcodes

This module obviously depends on the shortcodes module.


# Install
`composer require drupal/advanced_shortcodes`
Enable the module.

# To use with CKEditor
To use the new shortcodes defined here, you might need to go to 
Admin menu
> Configuration
> Content Authoring
> Text formats and editors
> (Select the text format, e.g. Full HTML)
> Look at the shortcode settings, see the new codes, check them and save.
And clear cache.

# Tested shortcodes that working:
- Accordions / Accordion
- Column
- Icon
